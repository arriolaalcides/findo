<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport"
	content="width=device-width, initial-scale=1, user-scalable=yes">
	<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<link href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css" rel="stylesheet">
	<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <div class="navbar-header">
	      <a class="navbar-brand" href="{{ url('/') }}">Prepare your pizza</a>
	    </div>
	    <ul class="nav navbar-nav">
	      <li class="active"><a href="{{ url('/') }}">Home</a></li>
	      <li><a href="{{ url('/pizza') }}">Pizza</a></li>
	      <li><a href="{{ url('/ingredient') }}">Ingredient</a></li>
	    </ul>
	  </div>
	</nav>

	<div class="container-fluid" style="margin-top: 100px">

		@yield('content')
	</div>
	<style type="text/css">
	.table {
		border-top: 2px solid #ccc;

	}
</style>
</body>
</html>