@extends('layouts.layout')
@section('content')
<div class="row">
  <section class="content">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="pull-left"><h3>Catalog of pizzas</h3></div>
          <div class="pull-right">
            <div class="btn-group">
              <a href="{{ route('pizza.create') }}" class="btn btn-info" >Add pizza</a>
            </div>
          </div>
          <div class="table-container">
            <table id="pizza" class="table table-bordred table-striped">
             <thead>
               <th>Name</th>
               <th>Price</th>
               <th>View</th>               
               <th>Edit</th>
               <th>Delete</th>
             </thead>
             <tbody>
              @if($pizzas->count())  
              @foreach($pizzas as $pizza)  
              <tr>
                <td id="desc_{{$pizza->id}}">{{$pizza->description}}</td>
                <td id="price_{{$pizza->id}}">{{$pizza->price}}</td>
                <td>
                  <a class="btn btn-primary btn-xs" href="{{action('PizzaController@show', $pizza->id)}}" >
                    <span class="glyphicon glyphicon-eye-open"></span>
                  </a>
                </td>
                <td>
                  <button class="btn btn-primary btn-xs edit" value="{{$pizza->id}}" data-toggle="modal" data-target="#myEditModal">
                    <span class="glyphicon glyphicon-pencil"></span>
                  </button>
                </td>
                <td>
                  <form action="{{action('PizzaController@destroy', $pizza->id)}}" method="post">
                   {{csrf_field()}}
                   <input name="_method" type="hidden" value="DELETE">

                   <button class="btn btn-danger btn-xs" type="submit"><span class="glyphicon glyphicon-trash"></span></button>
                 </form>
                 </td>
               </tr>
               @endforeach 
               @else
               <tr>
                <td colspan="8">No pizza !!</td>
              </tr>
              @endif
            </tbody>

          </table>
        </div>
      </div>
      {{ $pizzas->links() }}
    </div>
  </div>
</section>

<div class="modal fade" id="myEditModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" align="center"><b>Edit Pizza</b></h4>
      </div>
      <div class="modal-body">
        <form method="POST" id="form-pizza" action="{{route('pizza.update',1)}}"  role="form">
              {{ csrf_field() }}
              <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                    <input type="text" name="description" id="description" class="form-control input-sm" placeholder="Description of the pizza"/>
                  </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6">
                  <div class="form-group">
                    <input type="number" step="any" name="price" id="price" class="form-control input-sm" placeholder="Price 0.00" pattern="^\d+(?:\.\d{1,2})?$"/>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                  <div class="form-group">
                  </div>
                  <!-- Begin Table -->
                  <div class="alert alert-danger" id="alert-ingredient" style="display: none">
                  </div>
                  <div class="table-container">
                          <table id="ingredient" class="table table-bordred table-striped">
                           <thead>
                             <th>Name</th>
                             <th>Price</th>                        
                             <th>
                                <input type="checkbox" class="form-check-input" name="ingredients_all" id="ingredients_all" value="">
                                Select all
                             </th>
                           </thead>
                           <tbody id="prices-data">
                            @if($ingredients->count())  
                            @foreach($ingredients as $ing)  
                            <tr>
                              <td>{{$ing->description}}</td>
                              <td class="price">{{$ing->price}}</td>
                              <td>
                                <input type="checkbox" class="form-check-input checked" name="ingredients[]" id="ingredients_{{$ing->id}}" value="{{$ing->id}}">
                              </td>
                             </tr>
                             @endforeach 
                             @else
                             <tr>
                              <td colspan="8">No ingredients !!</td>
                            </tr>
                            @endif
                          </tbody>

                        </table>
                      </div>
                  <!-- End Table -->
                </div>
              </div>
              <div class="row">
                <div class="col-xs-3 col-sm-3 col-md-3">
                  <div class="form-group">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                  </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-6"></div>
                <div class="col-xs-3 col-sm-3 col-md-3">
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                  </div>
                </div>
              </div>
            </form>
      </div>
    </div>
  </div>
</div>

<script src="{{ asset('js/pizza/pizza_update.js') }}"></script>
@endsection