@extends('layouts.layout')
@section('content')
<div class="row">
	<section class="content">
		<div class="col-md-8 col-md-offset-2">
			@if (count($errors) > 0)
			<div class="alert alert-danger">
				<strong>Error!</strong> Check the mandatory fields.<br><br>
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
			@endif
			@if(Session::has('success'))
			<div class="alert alert-info">
				{{Session::get('success')}}
			</div>
			@endif

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">New pizza</h3>
				</div>
				<div class="panel-body">					
					<div class="table-container">
						<form method="POST" id="form-pizza" action="{{ route('pizza.store') }}"  role="form">
							{{ csrf_field() }}
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text" name="description" id="description" class="form-control input-sm" placeholder="Description of the pizza"/>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="number" step="any" name="price" id="price" class="form-control input-sm" placeholder="Price 0.00" pattern="^\d+(?:\.\d{1,2})?$"/>
									</div>
								</div>
								<div class="col-xs-12 col-sm-12 col-md-12">
									<div class="form-group">
									</div>
									<!-- Begin Table -->
									<div class="alert alert-danger" id="alert-ingredient" style="display: none">
									</div>
									<div class="table-container">
							            <table id="ingredient" class="table table-bordred table-striped">
							             <thead>
							               <th>Name</th>
							               <th>Price</th>							           
							               <th>
							               		<input type="checkbox" class="form-check-input" name="ingredients_all" id="ingredients_all" value="">
							               		Select all
							               </th>
							             </thead>
							             <tbody id="prices-data">
							              @if($ingredients->count())  
							              @foreach($ingredients as $ing)  
							              <tr>
							                <td>{{$ing->description}}</td>
							                <td class="price">{{$ing->price}}</td>
							                <td>
							                  <input type="checkbox" class="form-check-input checked" name="ingredients[]" id="ingredients_{{$ing->id}}" value="{{$ing->id}}">
							                </td>
							               </tr>
							               @endforeach 
							               @else
							               <tr>
							                <td colspan="8">No ingredients !!</td>
							              </tr>
							              @endif
							            </tbody>

							          </table>
							        </div>
									<!-- End Table -->
								</div>
							</div>
							<div class="row">
								<div class="col-xs-3 col-sm-3 col-md-3">
									<div class="form-group">
										<a href="{{ route('pizza.index') }}" class="btn btn-info btn-block" >Back</a>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6"></div>
								<div class="col-xs-3 col-sm-3 col-md-3">
									<div class="form-group">
										<input type="submit"  value="Save" class="btn btn-success btn-block">
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>

			</div>
		</div>
	</section>
	<script src="{{ asset('js/pizza/pizza.js') }}"></script>
	@endsection
