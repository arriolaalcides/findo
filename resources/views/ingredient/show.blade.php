@extends('layouts.layout')
@section('content')
<div class="row">
	<section class="content">
		<div class="col-md-8 col-md-offset-2">
			
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Ingredient</h3>
				</div>
				<div class="panel-body">					
					<div class="table-container">
							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text" id="description" class="form-control input-sm" value="{{$ingredient->description}}" disabled>
									</div>
								</div>
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<input type="text" id="price" class="form-control input-sm" value="{{$ingredient->price}}" disabled>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="col-xs-6 col-sm-6 col-md-6">
									<div class="form-group">
										<a href="{{ route('ingredient.index') }}" class="btn btn-info btn-block col-xs-6 col-sm-6 col-md-6" >Back</a>
									</div>
								</div>	
							</div>
					</div>
				</div>

			</div>
		</div>
	</section>
	@endsection