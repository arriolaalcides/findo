@extends('layouts.layout')
@section('content')
<div class="row">
  <section class="content">
    <div class="col-md-8 col-md-offset-2">
      <div class="panel panel-default">
        <div class="panel-body">
          <div class="pull-left"><h3>Catalog of ingredients</h3></div>
          <div class="pull-right">
            <div class="btn-group">
              <a href="{{ route('ingredient.create') }}" class="btn btn-info" >Add ingredient</a>
            </div>
          </div>
          <div class="table-container">
            <table id="ingredient" class="table table-bordred table-striped">
             <thead>
               <th>Name</th>
               <th>Price</th>
               <th>View</th>               
               <th>Edit</th>
               <th>Delete</th>
             </thead>
             <tbody>
              @if($ingredients->count())  
              @foreach($ingredients as $ing)  
              <tr>
                <td>{{$ing->description}}</td>
                <td>{{$ing->price}}</td>
                <td>
                  <a class="btn btn-primary btn-xs" href="{{action('IngredientController@show', $ing->id)}}" >
                    <span class="glyphicon glyphicon-pencil"></span>
                  </a>
                </td>
                <td>
                  <a class="btn btn-primary btn-xs" href="{{action('IngredientController@edit', $ing->id)}}" >
                    <span class="glyphicon glyphicon-pencil"></span>
                  </a>
                </td>
                <td>
                  <form action="{{action('IngredientController@destroy', $ing->id)}}" method="post">
                     {{csrf_field()}}
                     <input name="_method" type="hidden" value="DELETE">

                     <button class="btn btn-danger btn-xs" type="submit">
                      <span class="glyphicon glyphicon-trash"></span>
                    </button>
                  </form>
                 </td>
               </tr>
               @endforeach 
               @else
               <tr>
                <td colspan="8">No ingredients !!</td>
              </tr>
              @endif
            </tbody>

          </table>
        </div>
      </div>
      {{ $ingredients->links() }}
    </div>
  </div>
</section>

@endsection