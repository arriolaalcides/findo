<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Calificacion;
use App\Materia;
use App\Alumno;

/*
ERROR: 0
SUCCESS: 1
*/

class CalificacionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = DB::table('calificacion')
            ->join('materia', 'calificacion.materia_id', '=', 'materia.id')
            ->join('alumno', 'calificacion.alumno_id', '=', 'alumno.id')
            ->select('alumno.nombreApellido',
                    'materia.nombreMateria',
                    'calificacion.nota')
            ->get();
            
        return $students;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Instanciamos la clase calificacion
        $calificacion = new Calificacion;

        $validate = $this->validateFields($request);

        if($validate['error']) return json_encode($validate);
        
        $calificacion->alumno_id = $request->alumnoId;
        $calificacion->materia_id = $request->materiaId;
        $calificacion->nota = $request->nota;
        
        try{
            $calificacion->save();
            return $this->message(_('Registro creado.'),1);
        } 
        catch (Exception $e){
            return $this->message(_($e->getMessage()),0);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Solicitamos al modelo la calificacion con el id solicitado por GET.

        $students = DB::table('calificacion')
            ->join('materia', 'calificacion.materia_id', '=', 'materia.id')
            ->join('alumno', 'calificacion.alumno_id', '=', 'alumno.id')
            ->select('alumno.nombreApellido',
                    'materia.nombreMateria',
                    'calificacion.nota')
            ->where('calificacion.id', '=', $id)
            ->get();

        if(count($students)==0) return $this->message(_('No existe calificacion con ese código.'),0);

        return $students;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            // Verifica si existe el registro calificación
            if($this->show($id)['error']) return $this->message(_('No existe registro.'),0);

            $calificacion = Calificacion::find($id);

            $validate = $this->validateFields($request);

            // Verifica que tanto alumno como materia exista su ID
            if($validate['error']) return json_encode($validate);

            $calificacion->alumno_id = $request->alumnoId;
            $calificacion->materia_id = $request->materiaId;
            $calificacion->nota = $request->nota;

            $calificacion->save();
            return $this->message(_('Registro actualizado.'),1);
        } 
        catch (Exception $e){
            return $this->message(_($e->getMessage()),0);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            
            if($this->show($id)['error']) return $this->message(_('No existe registro.'),0);

           $calificacion = Calificacion::destroy($id);
           return $this->message(_('Registro eliminado.'),1);
        } 
        catch (Exception $e){
            return $this->message(_($e->getMessage()),0);
        }
    }

    /**
     * Get average califications by subject ID.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function average($id)
    {
        $average = DB::table('calificacion')
            ->join('materia', 'calificacion.materia_id', '=', 'materia.id')
            ->select('materia.nombreMateria', 
                     DB::raw('AVG(calificacion.nota) as promedio'))
            ->where('materia.id', '=', $id)
            ->groupBy('calificacion.materia_id','materia.nombreMateria')
            ->get();
        
        if(count($average)==0) return $this->message(_('No existe el promedio para la materia.'),0);

        return $average;
    }

    /**
     * Get califications by student ID.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function calification($id)
    {
        $calificacion = DB::table('calificacion')
            ->join('alumno', 'calificacion.alumno_id', '=', 'alumno.id')
            ->select('alumno.nombreApellido', 
                     'calificacion.nota')
            ->where('calificacion.alumno_id', '=', $id)
            ->get();

        if(count($calificacion)==0) return $this->message(_('No existe las notas para este alumno.'),0);
            
        return $calificacion;
    }

    /**
     * Check records by its ID.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    private function validateFields(Request $request)
    {
        $alumno = Alumno::find($request->alumnoId);

        $error['error'] = false;

        if (NULL===$alumno){
            return $this->message(_("No existe alumno con ese código."),0);
        }

        $materia = Alumno::find($request->materiaId);

        if (NULL===$materia){
            return $this->message(_("No existe materia con ese código."),0);
        }

        return $error;
    }

    /**
     * Create a message.
     *
     * @param  string  $message
     * @return array
     */
    private function message($message, $value){
        $error['error'] = ($value==0)?true:false; 
        $error['mensaje'] = _($message);
        return $error;
    }

}
