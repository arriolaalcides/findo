<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
	public $table = "alumno";
    protected $fillable = ['nombreApellido'];
}
