<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calificacion extends Model
{
    public $table = "calificacion";
    protected $fillable = ['alumno_id','materia_id','nota'];
}
