## INSTRUCCIONES

1) Crear una base de datos "findo".

2) ejecutar archivo findo.sql

Ya posee registros.

3) clonar el repositorio.

4) ejecutar composer install.

5) ejecutar php artisan server.

6) Abrir una aplicaci�n para ejecutar APIs, ejemplo: �Postman.

7) Listado de URLs para �ste test:

POST | api/calificacion (Crear calificaciones, ver punto 8)

GET  | api/alumno (Ver listado de alumnos)

GET  | api/materia (Ver listado de materias)

GET  | api/calificacion (Listado, ver punto 9)

GET  | api/calificacion/average/{calificacion} (Promedio por materia, ver punto 11)

GET  | api/calificacion/calification/{calificacion} (Nota por alumno, ver punto 10)


8) Crear un nuevo registro para calificacion:
[POST] 127.0.0.1:8000/api/calificacion/

Body > Raw
{
	"alumnoId":"1",\n
	"materiaId":"1",\n
	"nota":4\n
}

Repetir este paso las veces que sea necesario

9) Ver listado por:

[GET] 127.0.0.1:8000/api/calificacion/

10) Ver nota de calificaci�n por una alumno en particular:

[GET] 127.0.0.1:8000/api/calificacion/calification/1

11) Ver promedio por materia:

[GET] 127.0.0.1:8000/api/calificacion/average/1

