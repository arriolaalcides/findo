$(document).ready(function() {

    // Sum all prices on the table
    $('#ingredients_all').on("click", function(){
        
        $('#alert-ingredient').hide();

        if( $(this).is(':checked') ) 
        {
            $('.checked').prop('checked', true);

            sumTotal($('.price'));
        }
        else
        {
            $('.checked').prop('checked', false);
            $('#price').val(0);
        }  
    });
    
    // By every check, sums its prices
    $('#prices-data').on('change', 'input[type="checkbox"]', function(){

        var price = parseFloat($(this).parents("tr").find("td").eq(1).text());

        var totalSumValue = ($('#price').val().length > 0)? parseFloat($('#price').val()) : 0;

        $('#alert-ingredient').hide();

        if ($(this).is(':checked')) 
        {
            totalSumValue += price;
        } 
        else
        {
            totalSumValue -= price;
        }

        $('#price').val(totalSumValue.toFixed(2));
    });

    // Validate mandatory fields and choice at last one ingredient
    $('#form-pizza').on('submit', function(e){
        
        var checkAllLength = checkAllIngredient($('.checked'));

        if (checkAllLength==0) 
        {
            $('#alert-ingredient').show();
            $('#alert-ingredient').text('You must choose at last one ingredient.');
            return false;
        }
        
    });

} );

// Every time checkbox 'select' is checked, returns all sum
function sumTotal(varClass)
{
    var sum=0;

    varClass.each(function() {
        sum += parseFloat($(this).text());
    });
    
    $('#price').val(sum.toFixed(2));
}

// Checks if at last one ingredient was chosen
function checkAllIngredient(varClass)
{
    var checked=0;

    varClass.each(function() {
        if($(this).is(':checked')) return checked = 1;
    });
    
    return checked;
}